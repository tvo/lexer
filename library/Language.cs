namespace lexer
{
	public class Language
	{
		public Language()
		{
		}

		// usage notes: this adds primitives for parsing the base text stuff.
		// Example AddTerm("[\\d]+", "digit") for a digit
		public void AddTerm(string pattern, string name)
		{
		}

		// called once. This makes optimizations for running the language.
		public void Compile()
		{
		}

		public void Evaluate(string input)
		{
		}
	}
}
